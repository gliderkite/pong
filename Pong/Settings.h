//
//  Settings.h
//  pong
//
//  Created by Marco Conte on 06/12/14.
//  Copyright (c) 2014 Marco Conte. All rights reserved.
//

#ifndef pong_Settings_h
#define pong_Settings_h


namespace pong {
	
	static const unsigned FRAME_RATE = 60;
	
	static const unsigned WINDOW_WIDTH = 640;
	static const unsigned WINDOW_HEIGHT = 480;
	
	static const const char* WINDOW_TITLE = "PONG";
	
	static const unsigned GOAL = 5;
	
	static const float AI_MIN_DELAY = 100.f;
	static const float AI_MAX_DELAY = 200.f;
	
	static const float BALL_SIZE = 15.f;
	
	static const float PLAYER_WIDTH = 15.f;
	static const float PLAYER_HEIGHT = 70.f;
	static const float PLAYER_ABSCISSA = 10.f;
	static const float PLAYER_SPEED_FACTOR = 0.5f;
	static const float PLAYER_POS_UNCERTAINTY = 5.f;
	
	static const const char* FONT_FILENAME = "minecrafter.ttf";
	static const const char* PLOP_SOUND_FILENAME = "plop.ogg";
	static const const char* BEEP_SOUND_FILENAME = "beep.ogg";
	static const const char* ICON_FILENAME = "pong128.png";
	
	static const float BALL_MAX_SPEED = 0.65f;
	static const float BALL_MIN_SPEED = 0.35f;
	
	static const unsigned SEPARATORS_NUMBER = 10;
	static const float SEPARATOR_WIDTH = 6.f;
	static const float SEPARATOR_HEIGHT = 24.f;
	static const float SEPARATOR_DIST = 64.f;
	
	static const unsigned FONT_SCORE_SIZE = 36;
	static const unsigned FONT_TITLE_SIZE = 128;
	static const unsigned FONT_START_SIZE = 30;
	static const unsigned FONT_TRIBUTE_SIZE = 20;
	static const unsigned FONT_WIN_SIZE = 58;
	
	static const float SCORE_ABSCISSA = 70.f;
	static const float SCORE_ORDINATE = 20.f;
	static const float TITLE_ORDINATE = 40.f;
	static const float START_ORDINATE = 250.f;
	static const float DEMO_ORDINATE = 300.f;
	static const float TRIBUTE_ORDINATE = 400.f;
	static const float WIN_ORDINATE = 100.f;
	static const float NEWGAME_ORDINATE = 200.f;

}


#endif
